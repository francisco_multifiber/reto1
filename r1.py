monedas = []


def otra():
    
    
    try:
        otra = input("Deseas ingresar otra moneda? [Y,n]: ")
    
        if otra.lower() in ['y', 'yes', '']:
            return True
        
        elif otra.lower() in ['n', 'no']:
            return False

        else: 
            print('Opción invalida')
            return otra()

    except Exception:
        print('Opción invalida')
    
        return otra()
    
    

def pedirDatos():

    try:
        while True:
            diametro = int(input("Introduce el Diametro de la moneda: "))
            
            monedas.append(diametro)

            if not otra():
                break

    except Exception:
        print('Valor incorrecto')
        pedirDatos()
    
def ordenar():
    # print('ordenar')
    cambio = False
            
    for i in range( 0, len(monedas) ):
        try:
            if ( not i == 0):
                # print(monedas[i])
                if ( monedas[i] > monedas[i-1] ):
                    m = monedas[i-1]
                    monedas[i-1] = monedas[i]
                    monedas[i] = m
                    cambio = True
                    print(monedas)

            if ( not i == len(monedas)-1):
                if ( monedas[i] < monedas[i+1] ):
                    m = monedas[i+1]
                    monedas[i+1] = monedas[i]
                    monedas[i] = m
                    cambio = True
                    print(monedas)    
        except Exception:
            pass
        
    # print(cambio)
    if cambio:
        ordenar()
    else:
        return

if __name__ == '__main__':
    pedirDatos()
                #  1 2 3 4 5
    # monedas = [3,5,2,4,1]

    print(monedas)

    ordenar()
